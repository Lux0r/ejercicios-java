package Logica.Ejercicio1;


import java.awt.event.*;
import javax.swing.*;


public class Frame extends JFrame{
	Frame(){
		
		setBounds(430,200,450,400);
		TextPanel spaceWork=new TextPanel();
		add(spaceWork);
		setVisible(true);
		pack();
	}
}

class TextPanel extends JPanel{
	
	public TextPanel(){
		
		JLabel indication=new JLabel();
		indication.setText("Ingresar los n�meros separados por espacios( ), luego presionar el boton Sumar");
		field=new JTextField(20);
		JButton button=new JButton("Sumar");
		add(indication);
		add(button);
		add(field);
		GetText event=new GetText();
		button.addActionListener(event);
		}
	
	private class GetText implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			textSpaces=field.getText();
			System.out.println("La suma total es :" + turnIntoString(textSpaces));
			field.setText(null);
		}
		
		public int turnIntoString(String text) {
			
			int result=0;
			String[] r=text.split(" ");

			for(int i=0;i<r.length;i++) {

				try{
						if(!r[i].isEmpty()) {
							result+=Integer.parseInt(r[i]);
						}

				}catch(Exception e1){
	
					System.out.println("ERROR, letras o caracteres especiales no se pueden Sumar!!!");

				}
			}
			return result;
		}
	}
	
	private String textSpaces;
	private JTextField field;
}



