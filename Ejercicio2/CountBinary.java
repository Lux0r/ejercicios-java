package Logica.Ejercicio2;

public class CountBinary {

	public CountBinary(int number) {
		
	}
	
	public static int countOnesOf(int number) {

        char[] binaryNumber = Integer.toBinaryString(number).toCharArray();
        int ones=0;

        for(char binaryChar : binaryNumber){
            if(binaryChar=='1') {
                ones++;
            }
        }

        return ones;
    }
}
