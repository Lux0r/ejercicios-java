package Logica.Ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {

        int number=-1;

        do{
       
            System.out.println("Ingrese el número de Dulces:");
            Scanner input=new Scanner(System.in);
            try {
                number=input.nextInt();
                input.close();
                if(number<0 || number>100) {
                	System.out.println("Debes escribir un número entre 0 y 100");
                	number=0;
                }
            } catch (Exception ex) {
                System.out.println("El caracter ingresado no es un número entero");
            }
        }while(number<0 || number>100);
        
        BestSum.bestResult(number);
        
    }
}