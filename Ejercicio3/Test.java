package Logica.Ejercicio3;

public class Test {

    public static String makeTest(String text){

        int step=0,count=0;

        char[] arrayTest= new char[]{'C', 'I', 'D', 'E', 'N', 'E', 'T'};

        char[] arrayText= text.toCharArray();

        for(int i=0;i<arrayText.length;i++){

                if(arrayTest[0]==arrayText[i] && step==0){

                    arrayText[i]=' ';
                    step=1;
                    i=0;
                }
                else if(arrayTest[1]==arrayText[i] && step==1) {

                    arrayText[i]=' ';
                    step=2;
                    i=0;
                }
                else if(arrayTest[2]==arrayText[i] && step==2) {

                    arrayText[i]=' ';
                    step=3;
                    i=0;
                }
                else if(arrayTest[3]==arrayText[i] && step==3) {

                    arrayText[i]=' ';
                    step=4;
                    i=0;
                }
                else if(arrayTest[4]==arrayText[i] && step==4) {

                    arrayText[i]=' ';
                    step=5;
                    i=0;
                }
                else if(arrayTest[5]==arrayText[i] && step==5) {

                    arrayText[i]=' ';
                    step=6;
                    i=0;
                }
                else if(arrayTest[6]==arrayText[i] && step==6) {

                    arrayText[i]=' ';
                    step=7;
                    i=0;
                }
                else if(step==7) {

                    count+=1;
                    step=0;
                    i=0;
                }

        }
        return String.valueOf(count);
    }
}

